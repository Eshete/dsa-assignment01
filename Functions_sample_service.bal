import ballerina/grpc;
import ballerina/io;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "Functions" on ep {

    remote function new(loop value) returns updateInfo|error {
io:println("received an RPC request from a client...");
return {userid: value.full_Name};
}
}
    


