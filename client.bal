import ballerina/test;
import ballerina/http;

http:Client stClient = check new ("http://localhost:8080");


# + return - 
@test:Config {}
function exist() returns error? {
    
    http:Response response = check Client->get("/Students/student");
   
    test:assertEquals(response.statusCode, 200, 
    "code #error.");
   
    test:assertEquals(response.getJsonPayload(), {"id": "Nust22108", 
    "fullname": "Creater", "current": false}, 
    "No Record found matching you request");
}
