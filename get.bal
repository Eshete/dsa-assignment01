import ballerina/test;
import ballerina/http;

http:Client Client= check new ("http://localhost:8080");


# + return - 
@test:Config {}
function NotFound() returns error?{
   http:Response response = check Client->get("/student/Creater");

   test:assertEquals(response.statusCode, 404, 
      "The response contains unexpected status code.");

   test:assertEquals(response.getTextPayload(), 
      "No profile matches info provided");
}
