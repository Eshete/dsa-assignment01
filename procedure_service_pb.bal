import ballerina/grpc;

public isolated client class FunctionsClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function new(loop|ContextLoop req) returns (updateInfo|grpc:Error) {
        map<string|string[]> headers = {};
        loop message;
        if (req is ContextLoop) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Functions.Functions/new", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <updateInfo>result;
    }

    isolated remote function newContext(loop|ContextLoop req) returns (ContextUpdateInfo|grpc:Error) {
        map<string|string[]> headers = {};
        loop message;
        if (req is ContextLoop) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Functions.Functions/new", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <updateInfo>result, headers: respHeaders};
    }
}

public client class FunctionsUpdateInfoCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendUpdateInfo(updateInfo response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextUpdateInfo(ContextUpdateInfo response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextUpdateInfo record {|
    updateInfo content;
    map<string|string[]> headers;
|};

public type ContextLoop record {|
    loop content;
    map<string|string[]> headers;
|};

public type updateInfo record {|
    string language = "";
    string keywords = "";
    string full_Name = "";
    string email = "";
|};

public type loop record {|
    string 'function = "";
    string 'version = "";
|};

const string ROOT_DESCRIPTOR = "0A1770726F6365647572655F736572766963652E70726F746F120946756E6374696F6E73223C0A046C6F6F70121A0A0866756E6374696F6E180120012809520866756E6374696F6E12180A0776657273696F6E180220012809520776657273696F6E22770A0A757064617465496E666F121A0A086C616E677561676518032001280952086C616E6775616765121A0A086B6579776F72647318042001280952086B6579776F726473121B0A0966756C6C5F4E616D65180120012809520866756C6C4E616D6512140A05656D61696C1802200128095205656D61696C323A0A0946756E6374696F6E73122D0A036E6577120F2E46756E6374696F6E732E6C6F6F701A152E46756E6374696F6E732E757064617465496E666F620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"procedure_service.proto": "0A1770726F6365647572655F736572766963652E70726F746F120946756E6374696F6E73223C0A046C6F6F70121A0A0866756E6374696F6E180120012809520866756E6374696F6E12180A0776657273696F6E180220012809520776657273696F6E22770A0A757064617465496E666F121A0A086C616E677561676518032001280952086C616E6775616765121A0A086B6579776F72647318042001280952086B6579776F726473121B0A0966756C6C5F4E616D65180120012809520866756C6C4E616D6512140A05656D61696C1802200128095205656D61696C323A0A0946756E6374696F6E73122D0A036E6577120F2E46756E6374696F6E732E6C6F6F701A152E46756E6374696F6E732E757064617465496E666F620670726F746F33"};
}

