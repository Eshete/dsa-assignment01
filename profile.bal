import ballerina/io;
import ballerina/http;


type StdntProfile record {|

//int  st_no;
string st_name;
string st_lname;
//string preferred_format;
//json total_mdls;
 |};
    
    


StdntProfile[] all_stdnts = [];
service /stdnts on new http:Listener(8080) {
resource function get all() returns StdntProfile[] {
io:println("Retrieving Students profile database /students/all");
return all_stdnts;
}
resource function post insert(@http:Payload StdntProfile new_stdnt) returns json {
io:println("Appending to profile database /Student_Profile/insert");
all_stdnts.push(new_stdnt);
return {done: "Record Added Successfully"};
}
}