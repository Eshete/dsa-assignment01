import ballerina/http;




type profile record {
     string id;
     string fullname;
    boolean current;
};
   
   

configurable int port = 8080;
configurable string hostEp = "localhost";

listener http:Listener stProfile = new (port, config = {host: hostEp});

service / on stProfile  {

    private map< profile> st_profile = {"Stundent": {id: "@22180", fullname: 
        "creater", current: false}};

    resource function get  profile /[string st_Id]() returns @http:Payload  profile|
        http:BadRequest|http:NotFound {
        profile?  pro = self. st_profile[st_Id];
        if (pro is  profile) {
            return pro;
        } else {
            http:NotFound response = {body: "o record matches your request" + 
            st_Id};
            return response;
        }
    }

   
    resource function post pro (@http:Payload profile payload) returns @http:Payload profile|
        http:MethodNotAllowed|http:BadRequest {
        if (self.st_profile[payload.id] is ()) {
            self.st_profile[payload.id] = payload;
            return payload;
        } else {
            http:MethodNotAllowed res = {body: "Profile does not exist."};
            return res;
        }
        

        }
}

    
    

        

    


